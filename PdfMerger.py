import os

from Util import Log
from PyPDF2 import PdfFileReader, PdfFileWriter


def merge_pdfs(pdf_1, pdf_2, pdf_output):
    Log.info(f"try to merge: {os.path.basename(pdf_1)}, {os.path.basename(pdf_2)}")

    input_streams = [open(f, 'rb') for f in [pdf_1, pdf_2]]
    try:
        reader_f = PdfFileReader(input_streams[0], 'rb')
        reader_b = PdfFileReader(input_streams[1], 'rb')
        writer = PdfFileWriter()

        total_pages = reader_f.getNumPages()
        fn = range(0, total_pages)
        bn = reversed(fn)
        pairs = zip(fn, bn)

        for fn, bn in pairs:
            writer.addPage(reader_f.getPage(fn))
            writer.addPage(reader_b.getPage(bn).rotateClockwise(180))

        with open(pdf_output, "wb") as of:
            writer.write(of)
    finally:
        for s in input_streams:
            s.close()

    Log.info(f"merged to: {pdf_output}")
