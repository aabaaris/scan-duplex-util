import os
import inspect



class Log:
    @staticmethod
    def __log(msg_type, msg):
        filename = os.path.basename(inspect.currentframe().f_back.f_back.f_code.co_filename)
        os.system(f"log_tool -t {msg_type} -a '[{filename}] {str(msg)}'")

    @staticmethod
    def info(msg):
        Log.__log(0, msg)

    @staticmethod
    def warn(msg):
        Log.__log(1, msg)

    @staticmethod
    def error(msg):
        Log.__log(2, msg)


rw = {
    os.W_OK: 'writeable',
    os.R_OK: 'readable',
    os.X_OK: 'executable'
}


def check_is_dir(*paths, failure, mode: int):
    for p in paths:
        if not os.path.isdir(p):
            failure(f"Directory expected, given: '{p}'")
        if not os.access(p, mode):
            failure(f"Directory is not {rw[mode]}: '{p}'")
