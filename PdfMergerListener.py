import os
import pyinotify
import argparse
import glob

from Util import Log, check_is_dir
from PdfMerger import merge_pdfs


class PdfMergerHandler(pyinotify.ProcessEvent):

    def __init__(self, source_dir: str, output_dir: str):
        self.source_dir = source_dir
        self.output_dir = output_dir

    def process_IN_CLOSE_WRITE(self, event):
        Log.info(event)
        self.check_and_merge()

    def check_and_merge(self):
        files = glob.glob(f"{self.source_dir}/*.pdf")
        files.sort()
        files.sort(key=os.path.getmtime)

        if len(files) > 1:
            pdf_1, pdf_2 = files[0], files[1]
            pdf_output = os.path.join(self.output_dir, os.path.basename(pdf_2))

            merge_pdfs(pdf_1, pdf_2, pdf_output)

            os.remove(pdf_1)
            os.remove(pdf_2)

            self.check_and_merge()


def abbort(msg):
    print(msg)
    Log.error(msg)
    exit(-1)


def main():
    parser = argparse.ArgumentParser(description='Starts pdf-merger service')
    parser.add_argument("-sd", "--source-dir", help="source pdf directory", required=True)
    parser.add_argument("-od", "--output-dir", help="output pdf directory", required=True)
    args = parser.parse_args()

    check_is_dir(args.source_dir, args.output_dir, failure=lambda msg: abbort(msg), mode=os.X_OK)

    pdf_merger_handler = PdfMergerHandler(args.source_dir, args.output_dir)

    Log.info(f"start watching '{args.source_dir}', waiting for event 'pyinotify.IN_CLOSE_WRITE'")
    wm = pyinotify.WatchManager()
    wm.add_watch(args.source_dir, pyinotify.IN_CLOSE_WRITE, rec=True)
    pyinotify.Notifier(wm, pdf_merger_handler).loop()


if __name__ == '__main__':
    main()
